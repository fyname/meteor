/****************************************************
 * Description: DAO for t_sec_role_privilege
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-18 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sec.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sec.entity.RolePrivilegeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RolePrivilegeDao  extends XjjDAO<RolePrivilegeEntity> {
	public RolePrivilegeEntity getByRolePri(@Param("roleId") Long roleId, @Param("pcode") String pcode);

	/**
	 * 角色查询有权浏览（list）的菜单
	 * @param roleIds
	 * @return
	 */
	public List<RolePrivilegeEntity> findListByRoleHasListPri(@Param("roleIds") Long[] roleIds,@Param("listCode") String listCode);
}


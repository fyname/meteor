/****************************************************
 * Description: Service for 用户角色
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author RY
 * @version 1.0
 * @see
HISTORY
 *  2018-04-18 RY Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.sec.service;

import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.sec.entity.UserRoleEntity;

public interface UserRoleService extends XjjService<UserRoleEntity> {

    void deleteBy2Id(Long userId, Long roleId);

    void deleteAndSave(Long userId, Long[] roleIds);


}

package cn.com.ry.framework.application.meteor.sys.code.service.impl;

import cn.com.ry.framework.application.meteor.sys.code.dao.CodeDao;
import cn.com.ry.framework.application.meteor.sys.code.entity.ColumnInfo;
import cn.com.ry.framework.application.meteor.sys.code.entity.TableInfo;
import cn.com.ry.framework.application.meteor.sys.code.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CodeServiceImpl implements CodeService {


	@Autowired
	private CodeDao codeDao;

	public List<String> findTableList()
	{
		return codeDao.findTableList();
	}
	public List<ColumnInfo> findColumnsByTable(String tableName)
	{
		String dbName = codeDao.getDataBaseName();

		return codeDao.findColumnsByTable(tableName,dbName);
	}

	public TableInfo getTableInfoByName(String tableName)
	{
		String dbName = codeDao.getDataBaseName();
		return codeDao.getTableInfoByName(tableName,dbName);
	}
}

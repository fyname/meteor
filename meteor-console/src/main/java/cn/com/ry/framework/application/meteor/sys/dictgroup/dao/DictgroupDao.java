/****************************************************
 * Description: DAO for 字典组管理
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sys.dictgroup.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sys.dictgroup.entity.DictgroupEntity;


public interface DictgroupDao  extends XjjDAO<DictgroupEntity> {

}


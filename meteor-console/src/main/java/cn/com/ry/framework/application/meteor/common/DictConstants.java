package cn.com.ry.framework.application.meteor.common;

import cn.com.ry.framework.application.meteor.framework.XJJConstants;
import cn.com.ry.framework.application.meteor.framework.spring.SpringBeanLoader;
import cn.com.ry.framework.application.meteor.sys.dict.entity.DictEntity;
import cn.com.ry.framework.application.meteor.sys.dict.service.DictService;

import java.util.ArrayList;
import java.util.List;


public class DictConstants extends XJJConstants {

    private volatile static List<DictEntity> DICT_CACHE_LIST = new ArrayList<>();
    /**
     * 自定义字典组
     */
    public static final String DICT_GENDER = "gender";
    public static final String DICT_PROVINCE = "province";

    public static final String[] DICT_GROUP = {DICT_GENDER, DICT_PROVINCE};


    /**
     * 重新加载所有字典缓存
     */
    public synchronized static void init() {
        DictService dictService = (DictService) SpringBeanLoader.getBean("dictServiceImpl");
        DICT_CACHE_LIST.clear();
        DICT_CACHE_LIST .addAll(dictService.findAll());
    }

    /**
     * 根据组code和字典code获得名称
     *
     * @param groupCode
     * @param dictCode
     */
    public synchronized static String getDictName(String groupCode, String dictCode) {
        for (int i = 0; i < DICT_CACHE_LIST.size(); i++) {
            String key = DICT_CACHE_LIST.get(i).getGroupCode() + "_" + DICT_CACHE_LIST.get(i).getCode();
            if ((groupCode + "_" + dictCode).equals(key)) {
                return DICT_CACHE_LIST.get(i).getName();
            }
        }
        return null;
    }

    /**
     * 获取参数列表
     *
     * @param groupCode
     * @return
     */
    public synchronized static List<DictEntity> getDictList(String groupCode) {
        List<DictEntity> result = new ArrayList<>();
        for (int i = 0; i < DICT_CACHE_LIST.size(); i++) {
            String key = DICT_CACHE_LIST.get(i).getGroupCode();
            if (null != key && groupCode.equals(key) && DICT_CACHE_LIST.get(i).getStatus().equals("valid")) {
                result.add(DICT_CACHE_LIST.get(i));
            }
        }
        return result;
    }
}
